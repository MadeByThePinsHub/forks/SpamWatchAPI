# Security Policy

## Supported Versions

Because SpamWatch API is under in beta phrase, we're open for security vulnerabilities in the following
supported versions.

| Version | Supported          |
| ------- | ------------------ |
| 0.1.0   | :white_check_mark: |

## Reporting a Vulnerability

To report a vulnerability in the API server, use the `Report a Vulnerability` template
in creating a new issue in GitHub or GitLab, and we'll hide it from the public as possible
as we can.
