# SpamWatch API Server-side Docs
This is where the server-side docmentation lives in here. Currently Work-in-Progress and managed by @AndreiJirohHaliliDev2006.

## Building the Site
To build the documentation site locally, install Python dependencies in `requirements.txt` then run the following command in console.
Once builds are finished, you can now deploy it somewhere.

```bash
## Open this directory after cloning
cd docs && pip install -r requirements.txt

## build site locally
mkodcs build
```
