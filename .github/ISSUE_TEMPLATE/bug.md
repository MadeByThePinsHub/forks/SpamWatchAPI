---
name: Bug Report
about: Report a bug in the API server.
labels: ":bug: bug"
assignees: SitiSchu
---
## TL;DR
Summarize your bug report in 240 characters or less.

## About your Issue
Now, describe your issue in full detail.

## Workarounds
Describe what workarounds you did to trigger the bug.
