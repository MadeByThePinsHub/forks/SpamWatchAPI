---
name: Report a CI Issue
about: Report a bug in the API server.
labels: bug, enchancement
title: "[GITLAB CI ISSUE]: "
assignees: AndreiJirohHaliliDev2006
---
## TL;DR
Summarize your CI issue in 240 characters or less.

## CI Logs

Link to the CI logs should be enough

## CI Config

CI Configuration file should follows the template below.

```yml
## Replace this comment with your CI config contents.
```
